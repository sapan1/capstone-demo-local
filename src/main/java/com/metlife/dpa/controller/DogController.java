package com.metlife.dpa.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.metlife.dpa.model.Greeting;

@Controller     
public class DogController {

	@RequestMapping(value="/Dog",method=RequestMethod.GET)
	public String sayHello(Model model){
		
		model.addAttribute("formGreeting", "Hello oher Dogs: My name is Tomy!");
		return "dog";  
	} 
	
	@RequestMapping(value="/dog", method=RequestMethod.GET)
    public String greetingForm(Model model) {
    	Greeting greet=new Greeting();
    	greet.setContent("Great Britain");
        model.addAttribute("formGreeting", greet);
        return "doggreeting";  
    }

   
	
}
