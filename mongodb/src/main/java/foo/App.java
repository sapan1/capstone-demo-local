package foo;
import java.net.UnknownHostException;

import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.MongoClient;


public class App 
{

    public static void main( String[] args )throws UnknownHostException {
    
    	MongoClient mongoClient = new MongoClient();
    	 DB dbx1 = mongoClient.getDB("test");

         // Authenticate - optional
         // boolean auth = db.authenticate("foo", "bar");

         // Add some sample data
         DBCollection coll = dbx1.getCollection("aggregationExample");
         coll.insert(new BasicDBObjectBuilder()
                 .add("employee", 1)
                 .add("department", "Sales")
                 .add("amount", 71)
                 .add("type", "airfare")
                 .get());

       // System.out.println( "Hello World!" );
        dbx1.dropDatabase();
        mongoClient.close();
    }
}
